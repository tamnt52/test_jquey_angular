Rails.application.routes.draw do
  root :to => "front/templates#index"
  get '/chinchin' => 'front/templates#index'
  namespace :front do
    get '/tpl/:name.html' => 'templates#template'
    get '/tpl/:path1/:name.html' => 'templates#template'
    get '/tpl/:path1/:path2/:name.html' => 'templates#template'
  end
end
