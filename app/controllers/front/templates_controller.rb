class Front::TemplatesController < FrontController

  def index
    
    # ModelMailer.new_record_notification(@current_user).deliver
  end

  def template
    
    path = ''
    path = '/' + params[:path1] if params[:path1].present?
    path = path + '/' + params[:path2] if params[:path2].present?
    path = path + '/' + params[:name]
    # p path
    begin
      render template: "front/tpl/#{path}", layout: false
    rescue Exception => exception
      if Rails.env == 'development'
        p exception.message
        render text: exception.message
      else
        render text: t('controllers.tpl.actions.index.template_not_found', :tpl_path => path)
      end
    end
  end





end