chinchin = angular.module('chinchin', [ 'ngRoute'])
#angular.module('ng').filter 'tel', ->
chinchin.config ($httpProvider) ->
  authToken = $("meta[name=\"csrf-token\"]").attr("content")
  $httpProvider.defaults.headers.common["X-CSRF-TOKEN"] = authToken

chinchin.config ($routeProvider, $locationProvider) ->
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
  $routeProvider.when '/', redirectTo: '/chinchin'
  $routeProvider.when '/chinchin', templateUrl: '/front/tpl/index.html', controller: 'ChinchinController'

# Makes AngularJS work with turbolinks.
$(document).on 'page:load', ->
  $('[ng-app]').each ->
    module = $(this).attr('ng-app')
    angular.bootstrap(this, [module])